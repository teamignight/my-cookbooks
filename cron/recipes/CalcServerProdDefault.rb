#
# Cookbook Name:: cron
# Recipe:: calcserverProdDefault
#

cron "archiveLogs" do
  hour "5"
  minute "0"
  command "/home/ubuntu/LogArchiveScript"
end

cron "clearOldLogs" do
  hour "7"
  minute "0"
  command "/home/ubuntu/LogCleanupScript"
end
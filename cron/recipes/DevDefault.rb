#
# Cookbook Name:: cron
# Recipe:: default
#

cron "clearDailyData" do
  hour "7"
  minute "0"
  command "curl -X POST  -H 'Content-Type: application/json; charset=utf-8' -d \"{\"msg\":{\"cityId\":\"0\",\"dailyDataSecretKey\":\"spduankzu7edb1eumkpd\"}}\" dev.chicago.ignight.com/ignight_server/data/clear"
end

cron "archiveLogs" do
  hour "5"
  minute "0"
  command "/home/ubuntu/LogArchiveScript"
end

cron "clearOldLogs" do
  hour "7"
  minute "0"
  command "/home/ubuntu/LogCleanupScript"
end
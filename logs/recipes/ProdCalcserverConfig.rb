template "/home/ubuntu/LogArchiveScript" do
  cookbook "logs"
  source "ProdCalcserverLogArchiveScript.cfg.erb"
  owner "ubuntu"
  group "root"
  mode 777
end

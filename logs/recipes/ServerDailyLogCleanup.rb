template "/home/ubuntu/LogCleanupScript" do
  cookbook "logs"
  source "ServerLogCleanup.cfg.erb"
  owner "ubuntu"
  group "root"
  mode 777
end

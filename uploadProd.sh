bucket='s3://prod.resources.ignight/'

#update prod properties files
aws s3 sync properties/prod/ $bucket'properties/'

#create zip of cookbooks
zip -r ../chef.zip ../my-cookbooks

#upload cookbook to prod s3 bucket
aws s3 cp ../chef.zip $bucket'chef/'

#clean up zip
rm ../chef.zip

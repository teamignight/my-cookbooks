#Create directory for log file
directory "/var/log" do
  owner "tomcat7"
  group "root"
  mode "0777"
  action :create
end

#Create log file
file "/var/log/ignight.log" do
  owner "tomcat7"
  group "root"
  mode "0777"
  action :create_if_missing
end

#Create directory for properties file
directory "/properties" do
  owner "tomcat7"
  group "root"
  mode "0777"
  action :create
end

#Create directory for credentials
directory "/usr/share/tomcat7/.aws" do
  owner "tomcat7"
  group "root"
  mode "0777"
  action :create
end

#Create directory for credentials
directory "/home/ubuntu/.aws" do
  owner "tomcat7"
  group "root"
  mode "0777"
  action :create
end
#
# script "setup-aws-cli" do
#   user "root"
#   code <<-EOH
#     sudo pip install awscli
#   EOH
# end

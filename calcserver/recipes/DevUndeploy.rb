#kill running calcserver process
execute "kill-calcserver-jar" do
  command "jps -v | grep 'name=calcserver'| awk -F ' ' '{print $1}' | xargs kill -15"
  action :run
end
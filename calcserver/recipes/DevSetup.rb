#Create directory for log file
directory "/var/log" do
  owner "root"
  group "root"
  mode "0777"
  action :create
end

#Create log file
file "/var/log/ignight.log" do
  owner "root"
  group "root"
  mode "0777"
  action :create_if_missing
end

#Create directory for properties file
directory "/properties" do
  owner "root"
  group "root"
  mode "0777"
  action :create
end

#Create directory for credentials
directory "/root/.aws" do
  owner "root"
  group "root"
  mode "0777"
  action :create
end

# script "setup-aws-cli" do
#   user "root"
#   code <<-EOH
#     sudo pip install awscli
#   EOH
# end
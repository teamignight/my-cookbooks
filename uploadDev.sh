bucket='s3://dev.resources.ignight/'

#update dev properties files
aws s3 sync properties/dev/ $bucket'properties/'

#create zip of cookbooks
zip -r ../chef.zip ../my-cookbooks

#upload cookbook to dev s3 bucket
aws s3 cp ../chef.zip $bucket'chef/'

#clean up zip
rm ../chef.zip
